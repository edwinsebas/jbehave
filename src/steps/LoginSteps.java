package steps;

import org.jbehave.core.annotations.*;
import org.junit.Assert;

import pageObjects.LoginHtml;

public class LoginSteps{
    private LoginHtml loginPage = new LoginHtml();
    @Given("la url $url")    
	public void givenLaurl(@Named("url") String url){
	     this.loginPage = new LoginHtml();
		 this.loginPage.openPage(url);
	}
	@When("ingrese el usuario $user y la contrasena $pass")
	public void whenIngreseElUsuariouserYLaContrasenapass(@Named("user") String user, @Named("pass") String pass){
		this.loginPage.doLogin(user, pass);
	}
	@Then("inicia sesion $status")
	public void thenIniciaSesionCorrectamente(@Named("status") String status){
		Assert.assertEquals(this.loginPage.urlChanged(), status);	
	}	
	@AfterScenario
	public void closeBrowser(){
		this.loginPage.closePage();
	}
	
}
