package steps;

import java.util.Arrays;
import java.util.List;
 
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;
 
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import steps.LoginSteps;
 
@RunWith(JUnitReportingRunner.class)
public class LoginRunner extends JUnitStories {
 
	public LoginRunner() {
		super();
		configuredEmbedder()
	        .embedderControls()
	        .doGenerateViewAfterStories(true)
	        .doIgnoreFailureInStories(false)
	        .doIgnoreFailureInView(true)
	        .doVerboseFailures(true)
	        .useThreads(2)
	        .useStoryTimeoutInSecs(60);
	}
 
	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new LoginSteps());
	}
 
	@Override
	protected List<String> storyPaths() {
		return Arrays.asList("features/Login.story");
	}
}