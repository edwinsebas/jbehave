package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginHtml {
	
	private WebDriver driver = null;	
	
	String urlLogin = "";
	
	//user input xpath
	private String userNameXpath = "//input[@name='user']";  
	//password input xpath
	private String passWordXpath = "//input[@name='password']";
	//login button  xpath
	private String loginXpath = "//input[@onclick='login()']";	
	
	public LoginHtml(){		
		String exePath = "drivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);               
	}
	
	public void openPage(String url){
		this.driver = new ChromeDriver(); 
        this.driver.get(url);
        this.urlLogin = url;
	}
	
	public void doLogin(String user, String pass){
		this.setUserName(user);
		this.setPassword(pass);
		this.clickLogin();
	}
	
	public void setUserName(String user){
		WebElement userName = this.driver.findElement(By.xpath(this.userNameXpath));
		userName.sendKeys(user);
	}
	
	public void setPassword(String pass){
		WebElement passWord = this.driver.findElement(By.xpath(this.passWordXpath));
		passWord.sendKeys(pass);
	}
	
	public void clickLogin(){
		WebElement login = this.driver.findElement(By.xpath(this.loginXpath));
		login.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	public String urlChanged(){		
		return this.urlLogin.equals(this.driver.getCurrentUrl())? "false" : "true";
	}
	
	public void closePage() {
		this.driver.close();
	}
	
	
}
